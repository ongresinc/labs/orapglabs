# Migrating ORA to PG using ora2pg with docker-compose

Lab with docker-compose for testing migration from Oracle to PostgreSQL using ora2pg. This docker-compose spins 3 services: oracle, ora2pg, nginx (for the report visualization) and postgres.

Start:

```
docker-compose up -d
```

Get bridge IPs:

```
DOCKER_ORA_IP=$(docker inspect -f "{{with index .NetworkSettings.Networks \"orapglabs_ora2pgnet\"}}{{.IPAddress}}{{end}}" oracle11g)

DOCKER_PG_IP=$(docker inspect -f "{{with index .NetworkSettings.Networks \"orapglabs_ora2pgnet\"}}{{.IPAddress}}{{end}}" postgres)
```

Clean:

```
docker-compose down && rm -rf data/ && rm -rd pgdata/
```

## Prerequisites

First, it is needed to unlock HR user in the Oracle instance:

```bash
docker-compose exec oracle11g /bin/bash -c  "sqlplus system/oracle@//localhost:1521/xe @/scripts/unlock_user.sql" 
```

It is not strictly necessary to use the client inside the ora2pg container, but if it is intended to do so, you'll need to install postgres clients:

```bash
docker-compose exec ora2pg /bin/bash -c "yum install -y postgresql.x86_64"
```

### Orafce

Most of the DBaaS have `orafce` already installed in their instances, which only requires to issue the `CREATE EXTESION`
to make it work. Although, as it is not a core extension, it needs to be explicitly installed in the Postgres docker
container:

```bash
docker-compose exec postgres bash -c "apt update && apt install -y postgresql-14-orafce"
```


## Ora2pg Configuration, test, and estimation

### Configure the Oracle connection


If the Oracle host is outside this docker network, it is necessary to change `/config_files/ora2pg.conf` connection variables as follows (ip_host, port, and sid):

```
ORACLE_DSN      dbi:Oracle:host=ip_host(XXX.XXX.XXX.XXX);sid=xe;port=1521
ORACLE_USER     HR
ORACLE_PWD      hr
```

### Test connection 

```bash
docker-compose exec ora2pg /bin/bash -c "ora2pg -t SHOW_VERSION -c /config_files/ora2pg.conf"
```

The outputs files will be in `/outputs/` folder.

### Estimation

ora2pg does as a custom assessment of the database schema complexity. To get this report, execute:

```bash
docker-compose exec ora2pg /bin/bash -c "ora2pg -t show_report --estimate_cost --dump_as_html -c /config_files/ora2pg.conf >/outputs/report_estimate.html"
```

This docker has an nginx service that exposes a web server to port 8000 for report visualization, you can use either `localhost:8000` or `IP:8000`.


## Schema migration to output files

Tables:

```bash
docker-compose exec ora2pg /bin/bash -c "ora2pg -t TABLE -o /outputs/1_tables_constraints.sql -c /config_files/ora2pg.conf "
```

Sequences:

```bash
docker-compose exec ora2pg /bin/bash -c "ora2pg -t SEQUENCE -o /outputs/2_sequences.sql -c /config_files/ora2pg.conf "
```

Views:

```bash
docker-compose exec ora2pg /bin/bash -c "ora2pg -t VIEW -o /outputs/3_views.sql -c /config_files/ora2pg.conf "
```

Procedures:

```bash
docker-compose exec ora2pg /bin/bash -c "ora2pg -t PROCEDURE -o /outputs/4_procedure.sql -c /config_files/ora2pg.conf "
```

Triggers:

```bash
docker-compose exec ora2pg /bin/bash -c "ora2pg -t TRIGGER -o /outputs/5_trigger.sql -c /config_files/ora2pg.conf "
```

## Load output files to Postgres 

It is posssible to access resources from within the docker network or through the bridge ip created by docker. 

e.g., for accessing through local tools (not through docker-compose neither docker exec):

```bash
psql -h ${DOCKER_PG_IP} -p 5432 -U postgres ...
```


### Creating exported users and databases


```
docker-compose exec ora2pg /bin/bash -c "psql -h postgres -U postgres -p 5432 -f  /outputs/db_and_user.sql"
```

Or, if accessing without docker:

```bash 
psql -h ${DOCKER_PG_IP} -p 5432 -U postgres -f  ora2pg/outputs/db_and_user.sql
```

### Loading objects

```bash
docker-compose exec ora2pg /bin/bash -c "export PGPASSWORD='hr' && psql -h postgres -U hr -p 5432 -f  /outputs/1_tables_constraints.sql"
docker-compose exec ora2pg /bin/bash -c "export PGPASSWORD='hr' && psql -h  postgres -U hr -p 5432 -f  /outputs/2_sequences.sql"
docker-compose exec ora2pg /bin/bash -c "export PGPASSWORD='hr' &&  psql -h postgres -U hr -p 5432 -f  /outputs/3_views.sql"
docker-compose exec ora2pg /bin/bash -c "export PGPASSWORD='hr' && psql -h postgres -U hr -p 5432 -f  /outputs/4_procedure.sql"
docker-compose exec ora2pg /bin/bash -c "export PGPASSWORD='hr' && psql -h postgres -U hr -p 5432 -f  /outputs/5_trigger.sql"
```

```bash
psql -h ${DOCKER_PG_IP} -p 5432 -U hr hr -f ora2pg/outputs/1_tables_constraints.sqlhr 
psql -h ${DOCKER_PG_IP} -p 5432 -U hr hr -f ora2pg/outputs/2_sequences.sqlhr 
psql -h ${DOCKER_PG_IP} -p 5432 -U hr hr -f ora2pg/outputs/3_views.sql
psql -h ${DOCKER_PG_IP} -p 5432 -U hr hr -f ora2pg/outputs/4_procedure.sqlhr 
psql -h ${DOCKER_PG_IP} -p 5432 -U hr hr -f ora2pg/outputs/5_trigger.sql
```

Once the trigger has been manually fixed, you can create the object by:

```bash
docker-compose exec ora2pg /bin/bash -c "export PGPASSWORD='hr' && psql -h postgres -U hr -p 5432 -f  /outputs/6_pg_trigger_solve.sql"
```

### Loading data 

```
docker-compose exec ora2pg /bin/bash -c "ora2pg -t COPY -o /outputs/data.sql  -c /config_files/ora2pg.conf"
```



### Migrating the data to PostgreSQL database directly 

If you want load data directly to PostgreSQL database then modify the file `config_files/ora2pg.conf` the PG connection variables (`require perl-DBD-Pg`):

```
PG_DSN		dbi:Pg:dbname=pgdb_name;host=ip_host;port=dbport
PG_USER	    usr_pg
PG_PWD		usr_pass
 

```
Or use the CLI options

```
 docker-compose exec ora2pg /bin/bash -c 'ora2pg -t COPY --pg_dsn "dbi:Pg:dbname=hr;host=YYY.YYY.YYY.YYY;port=5432" --pg_user "hr" --pg_pwd "hr"  -c /config_files/ora2pg.conf'
```

*Note: make sure the values of the followings options in the config file are 1:*

```
DROP_FKEY	1
DISABLE_TRIGGERS 1
```

### Setting Sequences values 

```
docker-compose exec ora2pg /bin/bash -c "sqlplus64 hr/hr@oracle11g:1521/xe @/config_files/get_seq_data.sql"
docker-compose exec ora2pg /bin/bash -c "export PGPASSWORD='hr' &&  psql -h postgres -U hr -d hr -p 5432 -f  /outputs/data_sequences.sql"
```

## Orafce

Create the extension in the `hr` database:

```bash
docker-compose exec postgres psql -Upostgres hr -c "CREATE EXTENSION orafce;"
```

Test the extension:


```sql
SELECT 'hello orafce' FROM DUAL;

SELECT ADD_MONTHS(DATE'2022/01/26',3) FROM DUAL;

select oracle.sysdate() ;

select dbms_output.enable();
select dbms_output.serveroutput('t');
select dbms_output.put_line('hello from dbms_output packages using orafce');
```

Using `dbms_output` message queue:

```sql
select dbms_output.put('hello from dbms_output packages using orafce');
select * from dbms_output.get_lines(1);
```


### Stop the container
```
sh clean.sh
```

